const express = require('express')
const fs = require('fs')

const app = express();


app.use(express.static("form"))

app.get("/produtos", (req, res) => {
    const json = JSON.stringify(req.query)

    fs.writeFileSync("./form/data.json", json, { encoding: 'utf8', flag: 'w' });

    res.send(req.query)
});

app.get("/salvos", (req, res) => {

    const data = fs.readFileSync("./form/data.json", "utf8");

    res.send(data)
});

app.listen(8080, () => {
    console.log("O servidor está online.")
})