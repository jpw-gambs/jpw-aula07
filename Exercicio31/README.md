### Exercicio 31

> Implemente um formulário HTML capaz de inserir produtos em uma lista e uma
> aplicação usando nodejs capaz de receber estes itens e salvá-los em um arquivo de
> texto. O formulário HTML também deve ser capaz de mostrar os itens
> armazenados no servidor. Utilize o método GET para receber dados do formulário
> fornecer os dados a serem inseridos

> Para rodar o servidor execute:

- _npm run start_

> Para ir para o formulário navague até o link:

- http://localhost:8080/index.html

  > Para ver o arquivo salvo navegue até o link:

- http://localhost:8080/data.json

> Para ver o conteudo salvo antes de cadastrar outros produtos utilize o botão buscarProdutos
> no formulário inicial.
